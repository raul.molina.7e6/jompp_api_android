package com.example.clases

import com.example.model.Foto
import com.example.model.Game
import com.example.model.Usuarios
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

class KotlinToJason {
    //añadir foto
    fun addFoto(foto: Foto){
        val archivoJson = File("src/main/kotlin/com/example/JSon/fotos.json")
        val fotoAñadir = Json.encodeToString(foto)
        archivoJson.appendText("$fotoAñadir\n")
    }
    //AÑADIR JUEGO A LISTA DE JUEGOS PARA JUGAR
    fun addNewGame(juegoParaAñadir: Game){
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json")
        val id = jsonToKotlin().getNextId()
        juegoParaAñadir.id = id
        val juego = Json.encodeToString(juegoParaAñadir)
        archivoJson.appendText("$juego\n")
    }
    //CAMBIAR JUEGO A JUEGO JUGADO
    fun toPlayedGame(id: String){
        val listaGames = mutableListOf<Game>()
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json")
        val lineasJson = archivoJson.readLines()
        for(i in lineasJson){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id == id)iToObject.jugado = true
            listaGames.add(iToObject)
        }
        archivoJson.writeText("")
        for(i in listaGames){
            val entrada = Json.encodeToString(i)
            archivoJson.appendText("$entrada\n")
        }
    }
    //AÑADIR USUARIO A LISTA DE USUARIOS
    fun addNewUser(user: Usuarios){
        val archivoJson = File("src/main/kotlin/com/example/JSon/usuarios.json")
        val newUser = Json.encodeToString(user)
        archivoJson.appendText("$newUser\n")
    }
    //ELIMINAR JUEGO POR ID
    fun deleteGameById(id: String){
        val listaGames = mutableListOf<Game>()
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json")
        val lineasJson = archivoJson.readLines()
        for(i in lineasJson){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id != id) listaGames.add(iToObject)
        }
        archivoJson.writeText("")
        for(i in listaGames){
            val entrada = Json.encodeToString(i)
            archivoJson.appendText("$entrada\n")
        }
        deleteFoto(id)
    }
    private fun deleteFoto(id: String){
        val listaFotos = mutableListOf<Foto>()
        val archivoJson = File("src/main/kotlin/com/example/JSon/fotos.json")
        val lineasJson = archivoJson.readLines()
        for(i in lineasJson){
            val iToObject = Json.decodeFromString<Foto>(i)
            if(iToObject.id == id){
                File(iToObject.ruta).delete()
            }


            if(iToObject.id != id) listaFotos.add(iToObject)
        }
        archivoJson.writeText("")
        for(i in listaFotos){
            val entrada = Json.encodeToString(i)
            archivoJson.appendText("$entrada\n")
        }
    }

    //AÑADIR JUEGO A FAVORITOS
    fun toFavourites(id: String){
        val listaGames = mutableListOf<Game>()
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json")
        val lineasJson = archivoJson.readLines()
        for(i in lineasJson){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id == id){
                iToObject.favorito = true
            }
            listaGames.add(iToObject)
        }
        archivoJson.writeText("")
        for(i in listaGames){
            val entrada = Json.encodeToString(i)
            archivoJson.appendText("$entrada\n")
        }
    }
    //QUITAR JUEGO DE FAVORITOS
    fun toUnFavourites(id: String){
        val listaGames = mutableListOf<Game>()
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json")
        val lineasJson = archivoJson.readLines()
        for(i in lineasJson){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id == id){
                iToObject.favorito = false
            }
            listaGames.add(iToObject)
        }
        archivoJson.writeText("")
        for(i in listaGames){
            val entrada = Json.encodeToString(i)
            archivoJson.appendText("$entrada\n")
        }
    }
    //PONER NOTA A JUEGO
    fun gameCalification(id: String, nota: String){
        val listaGames = mutableListOf<Game>()
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json")
        val lineasJson = archivoJson.readLines()
        for(i in lineasJson){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id == id){
                iToObject.puntuacion = nota.toInt()
            }
            listaGames.add(iToObject)
        }
        archivoJson.writeText("")
        for(i in listaGames){
            val entrada = Json.encodeToString(i)
            archivoJson.appendText("$entrada\n")
        }
    }
}