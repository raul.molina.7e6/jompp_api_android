package com.example.Routes

import com.example.clases.KotlinToJason
import com.example.clases.jsonToKotlin
import com.example.model.Game
import com.example.model.Usuarios
import com.example.model.jsonToMap
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.usersRoutes(){
    route("/usuarios"){
        authenticate("auth-digest") {
            get{
                return@get call.respond(true)
            }
        }

        //REGISTRAR USUARIO
        post {
            val newUser = call.receive<Usuarios>()
            if(jsonToKotlin().userAlredyExists(newUser.userName)){
                return@post call.respond(false)
            }

            KotlinToJason().addNewUser(newUser)
            jsonToMap()
            return@post call.respondText("true")
        }
    }

}

