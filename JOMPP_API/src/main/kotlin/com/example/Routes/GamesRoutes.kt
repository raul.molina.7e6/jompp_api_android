package com.example.Routes

import com.example.clases.KotlinToJason
import com.example.clases.jsonToKotlin
import com.example.model.Foto
import com.example.model.Game
import com.example.model.jsonToMap
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File

fun Route.gamesRouting(){
    authenticate("auth-digest") {
        route("/games"){
            //FOTOS
            get("/foto/{id?}"){
                val id = call.parameters["id"].toString()
                val foto = jsonToKotlin().getFoto(id)
                return@get call.respondFile(foto)
            }
            //LISTA DE JUEGOS PARA JUGAR PARA UN USUARIO
            get ("/{userName?}"){
                val userName = call.parameters["userName"]
                val listOfUserGames = jsonToKotlin().getAllGames(userName.toString())
                return@get call.respond(listOfUserGames)
            }
            //AÑADIR JUEGO A LA LISTA DE JUEGOS PARA JUGAR
            post {
                //val newGame = call.receive<Game>()
                val data = call.receiveMultipart()
                            val newGame = Game("","","","",false,"",0, false,false, "")
                val newFoto = Foto("", "")
                            data.forEachPart { part ->
                                when (part) {
                                    is PartData.FormItem -> {
                                        when(part.name){
                                            "id" -> newGame.id = part.value
                                            "nameUsuario" -> newGame.nameUsuario = part.value
                                            "name" -> newGame.name = part.value
                                            "genero" -> newGame.genero = part.value
                                            "multiplayer" -> newGame.multiplayer = part.value.toBoolean()
                                            "pagina" -> newGame.pagina = part.value
                                            "puntuacion" -> newGame.puntuacion = part.value.toInt()
                                            "favorito" -> newGame.favorito = part.value.toBoolean()
                                            "jugado" -> newGame.jugado = part.value.toBoolean()
                                        }
                                    }
                                    is PartData.FileItem ->{
                                        newFoto.ruta = "./images/${newGame.name}${newGame.nameUsuario}.jpg"
                                        val fileBytes = part.streamProvider().readBytes()
                                        File(newFoto.ruta).writeBytes(fileBytes)
                                    }
                                    else -> {}
                                }
                            }
                println(newGame.name)
                println(newFoto.ruta)
                val id = jsonToKotlin().getNextId()
                newFoto.id = id
                KotlinToJason().addFoto(newFoto)
                KotlinToJason().addNewGame(newGame)
                return@post call.respondText("hecho", status = HttpStatusCode.OK)
            }
            //LISTA DE JUEGOS JUGADOS
            get("/jugados/{userName?}") {
                val userName = call.parameters["userName"]
                val listOfUserGames = jsonToKotlin().getAllPlayedGames(userName.toString())
                return@get call.respond(listOfUserGames)
            }
            //LISTA DE JUEGOS FAVORITOS
            get("/favoritos/{userName?}") {
                val userName = call.parameters["userName"]
                val listOfFavourites = jsonToKotlin().getAllFavourites(userName.toString())
                return@get call.respond(listOfFavourites)
            }
            //DETALLE JUEGO
            get("/detalle/{id?}"){
                val id = call.parameters["id"]
                val detalles = jsonToKotlin().getOneGame(id.toString())
                //SI RETORNA UN OBJETO VACIO ES PORQUE ESA ID NO EXISTE pero es imposible porque
                //tiene que tener id si o si
                return@get call.respond(detalles)
            }
            //BORRAR JUEGO POR ID
            delete("{id?}"){
                val id = call.parameters["id"]
                if(!jsonToKotlin().gameExists(id.toString())){
                    return@delete call.respondText ("No se ha encontrado la id", status = HttpStatusCode.NotFound)
                }
                KotlinToJason().deleteGameById(id.toString())
                return@delete call.respondText("Borrado con exito", status = HttpStatusCode.OK)
            }
            //CAMBIAR JUEGO A JUEGO JUGADO
            put ("/jugado/{id?}"){
                val id = call.parameters["id"]
                if(!jsonToKotlin().gameExists(id.toString())){
                    return@put call.respondText ("No se ha encontrado la id", status = HttpStatusCode.NotFound)
                }
                KotlinToJason().toPlayedGame(id.toString())
                return@put call.respondText("Actualizado con exito", status = HttpStatusCode.OK)
            }
            //AÑADIR JUEGO A FAVORITOS
            put("/favorito/{id?}"){
                val id = call.parameters["id"]
                if(!jsonToKotlin().gameExists(id.toString())){
                    return@put call.respondText ("No se ha encontrado la id", status = HttpStatusCode.NotFound)
                }
                KotlinToJason().toFavourites(id.toString())
                return@put call.respondText("Actualizado con exito", status = HttpStatusCode.OK)
            }
            //QUITAR JUEGO DE FAVORITOS
            put("/nofavorito/{id?}"){
                val id = call.parameters["id"]
                if(!jsonToKotlin().gameExists(id.toString())){
                    return@put call.respondText ("No se ha encontrado la id", status = HttpStatusCode.NotFound)
                }
                KotlinToJason().toUnFavourites(id.toString())
                return@put call.respondText("Actualizado con exito", status = HttpStatusCode.OK)
            }
            //PONER NOTA A JUEGO
            put("/notajuego/{id?}/{nota?}"){
                val id = call.parameters["id"]
                val nota = call.parameters["nota"]
                if(!jsonToKotlin().gameExists(id.toString())){
                    return@put call.respondText ("No se ha encontrado la id", status = HttpStatusCode.NotFound)
                }
                KotlinToJason().gameCalification(id.toString(), nota.toString())
                return@put call.respondText("Puntuado con exito", status = HttpStatusCode.OK)
            }
        }
    }
}