package com.example

import com.example.model.jsonToMap
import com.example.plugins.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.cors.routing.*

fun main() {
    jsonToMap() //Meter en el mapa del auth digest todos los usuarios que estan en el json para poder hacer bien el login
    embeddedServer(Netty, port = 8080, host = "169.254.19.23", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    configureSecurity()
    configureSerialization()
    configureRouting()
    install(CORS) {
        //allowMethod(HttpMethod.Options)
        //        allowMethod(HttpMethod.Put)
        //        allowMethod(HttpMethod.Delete)
        //        allowMethod(HttpMethod.Patch)
        //        allowHeader(HttpHeaders.Authorization)
        //        allowHeader("MyCustomHeader")
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }
}
