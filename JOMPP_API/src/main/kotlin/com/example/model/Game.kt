package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Game(
    var id: String,
    var nameUsuario: String,
    var name: String,
    var genero: String,
    var multiplayer: Boolean,
    var pagina: String,
    var puntuacion: Int?,
    var favorito: Boolean, //false al principio
    var jugado: Boolean, //false al principio
    var URLimage: String
)