package com.example.jomppapp.API

import androidx.lifecycle.ViewModelProvider
import com.example.jomppapp.model.Game
import com.example.jomppapp.model.Usuarios
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Url
import java.io.File

class Repository(userName: String?, password: String?) {

    val apiInterface = APIinterface.create(userName, password)

    //FOTOS
    suspend fun getFotos(id: String) = apiInterface.getFotos("games/foto/$id")
    //LISTA DE JUEGOS PARA JUGAR PARA UN USUARIO
    suspend fun getJuegosNoJugados(userName: String?) = apiInterface.getJuegosNoJugados("games/$userName")
    //AÑADIR JUEGO A LA LISTA DE JUEGOS PARA JUGAR
    suspend fun addGameJuegosNoJugados(id: RequestBody, nameUsuario: RequestBody, name: RequestBody, genero: RequestBody, multiplayer: RequestBody, pagina: RequestBody, puntuacion: RequestBody, favorito: RequestBody, jugado: RequestBody, image: MultipartBody.Part) = apiInterface.addGameJuegosNoJugados(id, nameUsuario, name, genero, multiplayer, pagina, puntuacion, favorito,jugado, image)
    //LISTA DE JUEGOS JUGADOS
    suspend fun getJuegosJugados(userName: String?) = apiInterface.getJuegosJugados("games/jugados/$userName")
    //LISTA DE JUEGOS FAVORITOS
    suspend fun getJuegosFavoritos(userName: String?) = apiInterface.getJuegosFavoritos("games/favoritos/$userName")
    //DETALLE JUEGO
    suspend fun getJuego(id: String) = apiInterface.getJuego("games/detalle/$id")
    //BORRAR JUEGO POR ID
    suspend fun deleteGame(id: String) = apiInterface.deleteGame("games/$id")
    //CAMBIAR JUEGO A JUEGO JUGADO
    suspend fun gameToPlayedGames(id: String) = apiInterface.gameToPlayedGames("games/jugado/$id")
    //AÑADIR JUEGO A FAVORITOS
    suspend fun gameToFavourites(id: String) = apiInterface.gameToFavourites("games/favorito/$id")
    //QUITAR JUEGO DE FAVORITOS
    suspend fun gameToNoFavourites(id: String) = apiInterface.gameToNoFavourites("games/nofavorito/$id")
    //PONER NOTA A JUEGO
    suspend fun puntuarJuego(id: String, nota:String) = apiInterface.puntuarJuego("games/notajuego/$id/$nota")

    //LISTA DE PAGINAS DE JUEGOS
    //LISTA DE JUEGOS FILTRADA POR PAGINA DE DONDE PROVIENE

    //LOG IN DE USUARIO
    suspend fun getLogin() = apiInterface.getLogin("usuarios")
    //REGISTRAR USUARIO
    suspend fun registerUser(usuario: Usuarios) = apiInterface.registerUser(usuario)
}