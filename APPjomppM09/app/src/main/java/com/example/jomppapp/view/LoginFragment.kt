package com.example.jomppapp.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.jomppapp.R
import com.example.jomppapp.databinding.FragmentLoginBinding
import com.example.jomppapp.viewModel.GameViewModel


class LoginFragment : Fragment() {
    lateinit var gameViewModel: GameViewModel
    lateinit var binding: FragmentLoginBinding
    lateinit var myPreferences: SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        gameViewModel = ViewModelProvider(requireActivity())[GameViewModel::class.java]
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myPreferences = requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        setupForm()
        binding.loginButton.setOnClickListener {
            val userName = binding.emailLoginEditText.text.toString()
            val password = binding.passwordLoginEditText.text.toString()

            if(userName == ""){
                Toast.makeText(context, "No has escrito tu usuario", Toast.LENGTH_SHORT).show()
            }else if(password == ""){
                Toast.makeText(context, "No has escrito tu contraseña", Toast.LENGTH_SHORT).show()
            }else{
                if(gameViewModel.login(userName, password)){
                    gameViewModel.userName.value = userName
                    gameViewModel.password.value = password
                    rememberUser(binding.emailLoginEditText.text.toString(), binding.passwordLoginEditText.text.toString(), binding.rememberMe.isChecked)
                    findNavController().navigate(R.id.action_loginFragment_to_fragment1)
                }else{
                    Toast.makeText(context, "Usuario o contraseña incorrecto", Toast.LENGTH_SHORT).show()
                }
            }
        }
        binding.registrateButton.setOnClickListener {
            val userName = binding.emailLoginEditText.text.toString()
            val password = binding.passwordLoginEditText.text.toString()
            if(userName == ""){
                Toast.makeText(context, "No has escrito tu usuario", Toast.LENGTH_SHORT).show()
            }else if(password == ""){
                Toast.makeText(context, "No has escrito tu contraseña", Toast.LENGTH_SHORT).show()
            }else{
                if(gameViewModel.register(userName, password)){
                    gameViewModel.userName.value = userName
                    gameViewModel.password.value = password
                    rememberUser(binding.emailLoginEditText.text.toString(), binding.passwordLoginEditText.text.toString(), binding.rememberMe.isChecked)
                    findNavController().navigate(R.id.action_loginFragment_to_fragment1)
                }else{
                    Toast.makeText(context, "Ese usuario ya esta registrado", Toast.LENGTH_SHORT).show()
                }

            }

        }
    }

    private fun rememberUser(userName: String, pass: String, remember: Boolean) {
        if(remember){
            myPreferences.edit {
                putString("username", userName)
                putString("password", pass)
                putBoolean("remember", remember)
                putBoolean("active", remember)
                apply()
            }
        }else{
            myPreferences.edit {
                putString("username", "")
                putString("password", "")
                putBoolean("remember", remember)
                putBoolean("active", remember)
                apply()
            }
        }

    }
    private fun setupForm() {
        val username = myPreferences.getString("username", "")
        val pass = myPreferences.getString("password", "")
        val remember = myPreferences.getBoolean("remember", false)
        if (username != null) {
            if(username.isNotEmpty()){
                binding.emailLoginEditText.setText(username)
                binding.passwordLoginEditText.setText(pass)
                binding.rememberMe.isChecked = remember
            }
        }
    }


}