package API.JOMPP_API.src.main.kotlin.com.example.Routes

import com.example.clases.jsonToKotlin
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.pagesRouting(){
    //ESTO SE BORRA, SE PUEDE HACER EN EL ANDROID
    route("/pages"){
        //LISTA DE PAGINAS DE JUEGOS
        get {
            val listOfFavourites = jsonToKotlin().getPaginasDeJuegos()
            return@get call.respond(listOfFavourites)
        }
        //LISTA DE JUEGOS FILTRADA POR PAGINA DE DONDE PROVIENE
        get("/{pagina?}/{userName?}") {
            val pagina = call.parameters["pagina"]
            val userName = call.parameters["userName"]
            val listaFavoritosFiltrada = jsonToKotlin().getFilteredGames(pagina.toString(), userName.toString())
            return@get call.respond(listaFavoritosFiltrada)
        }
    }
}