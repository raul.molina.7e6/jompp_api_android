# JOMPP_RaulMolina_JoelMontalvan

## Descripción del Proyecto

JOMPP es una aplicación creada por Raul Molina y Joel Montalvan, estudiantes del Instituto Tecnológico de Barcelona (ITB). El nombre del juego es una combinación de palabras de "JUEGOS ONLINE de M**** PARA PC", se le puso dicho nombre debido a la cantidad de juegos de internet que jugaban los creadores para entretenerse durante las clases.

JOMPP es una app dedicada a guardar juegos de internet que el propio usuario introduce con la finalidad de no poder olvidarte de su nombre y página web destacada para acordarte de jugarlo posteriormente.

Su mecánica es muy simple: Cada vez que guardas un juego, el usuario es capaz de cambiarlo a diferentes listas dependiendo del estado que se situa el juego para el usuario, ya sea como "Para jugar", "Favoritos", "Jugados".
Puedes añadir y eliminar tantos juegos como quieras, incluso puedes ponerle una nota para valorarlo.

## Como instalar y ejecutar el proyecto

Primero de todo debe tener instalado en su pc los programas IntelliJ IDEA y Android Studio.
Para instalarlo debes hacer un 'git clone' en una carpeta de tu pc. A continuación se debe dirigir al archivo [APIinterface](https://gitlab.com/raul.molina.7e6/jompp_api_android/-/blob/master/APPjomppM09/app/src/main/java/com/example/jomppapp/API/APIinterface.kt) de la parte de android y a su vez al archivo [Application.kt](https://gitlab.com/raul.molina.7e6/jompp_api_android/-/blob/master/JOMPP_API/src/main/kotlin/com/example/Application.kt) de la parte de la API y cambiar unos numeros:

![img_1.png](img_1.png)
![img.png](img.png)
En ese lugar deberà poner su dirección ip para poder iniciar un servidor local y que pueda conectarse des del movil.

Una vez hecho esto, presiona el boton del play del intelliJ IDEA y espere a que se inicie el servidor,
a continuacion ve a Android Studio e inicia la aplicación y ya podras disfrutarla.

## Instrucciones del uso de la aplicación

Cuando inicies la app, lo primero que verás será la pantalla de login. Es sencillo, si no dispones de una cuenta te registras con tus datos, sino inicias normal, además dsispones de un boton switch para guardar tus datos y así no tener que escribir tus datos cada vez que entres.

Si no dispones de juegos añadidos, pulsa el botón (+) con el que introduciras todos los datos del juego para finalmente guardarlo.

Una vez guardado el juego, la aplicación puede distribuirlo de diferentes formas:

- JUEGO GUARDADO/ NO FAVORITO/ NO JUGADO -> Solo estara disponible en el apartado de JUGAR
- JUEGO GUARDADO/ FAVORITO/ NO JUGADO -> Solo estara disponible en el apartado de JUGAR y FAVORITO
- JUEGO GUARDADO/ FAVORITO/ JUGADO -> Solo estara disponible en el apartado de JUGADO y FAVORITO
- JUEGO GUARDADO -> Estara disponible en el apartado de su respectiva página WEB

## License

MIT License

Copyright (c) [2023] raul[itb]molina kind joel[itb]montalvan montiel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

