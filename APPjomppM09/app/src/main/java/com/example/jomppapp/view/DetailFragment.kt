package com.example.jomppapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.jomppapp.API.Repository
import com.example.jomppapp.Adapter.GameAdapter
import com.example.jomppapp.R
import com.example.jomppapp.databinding.FragmentDetailBinding
import com.example.jomppapp.databinding.FragmentDetailNoPlayedBinding
import com.example.jomppapp.viewModel.GameViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailFragment : Fragment() {
    private lateinit var gameAdapter: GameAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentDetailBinding
    lateinit var gameViewModel: GameViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        gameViewModel = ViewModelProvider(requireActivity())[GameViewModel::class.java]
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val genero = arguments?.getString("genero").toString()
        val pagina = arguments?.getString("pagina").toString()
        val multijugador = arguments?.getBoolean("multijugador")
        val nombre = arguments?.getString("nombre").toString()
        val id = arguments?.getString("id").toString()
        val puntuacion = arguments?.getInt("puntuacion")
        val fragmentProcedente = arguments?.getInt("fragmentProcedente")
        val nombrepagina = arguments?.getString("nombrepaginaproveniente3")
        val favorito = arguments?.getBoolean("favorito")

        CoroutineScope(Dispatchers.IO).launch {
            val response = Repository("admin", "password").getFotos(id)
            withContext(Dispatchers.Main){
                if(response.isSuccessful && response.body() != null){
                    val foto = response.body()!!.bytes()
                    Glide.with(requireContext())
                        .load(foto)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .circleCrop()
                        .into(binding.imagegameplayed)
                }
            }
        }

        binding.namegameplayed.text = nombre
        binding.generogameplayed.text = "Genero: $genero"
        binding.pagegameplayed.text = "Pagina: $pagina"
        if(multijugador == true){
            binding.multiplayeryesornot.text = "Multijugador"
        }else{
            binding.multiplayeryesornot.text = ""
        }
        var estrellas = ""
        for(i in 1..puntuacion!!){
            if(i <= puntuacion){
                estrellas += "⭐"
            }
            estrellas+= " "
        }
        binding.estrellas.text = estrellas

        if(favorito == true){
            binding.switchFavorito.isChecked = favorito
        }else{
            binding.switchFavorito.isChecked = false
        }

        binding.saveshangesbutton.setOnClickListener {
            if(binding.switchFavorito.isChecked){
                gameViewModel.favouriteGame(id)
            }else{
                gameViewModel.noFavouriteGame(id)
            }
        }

        binding.deletegameplayedbutton.setOnClickListener {
            gameViewModel.deleteJuego(id)
            val page = nombrepagina as String
            val action:NavDirections
            if(fragmentProcedente == 1){
                action = DetailFragmentDirections.actionDetailFragmentToFragment2()
            }else if(fragmentProcedente == 2){
                action = DetailFragmentDirections.actionDetailFragmentToFragment3()
            }else{
                action = DetailFragmentDirections.actionDetailFragmentToListOfFilteredGamesByPageFragment(page)
            }
            findNavController().navigate(action)
        }
    }

}