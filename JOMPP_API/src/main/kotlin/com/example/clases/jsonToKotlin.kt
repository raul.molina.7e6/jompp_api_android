package com.example.clases

import com.example.model.Foto
import com.example.model.Game
import com.example.model.Usuarios
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File

class jsonToKotlin {
    //FOTOS
    fun getFoto(id: String):File{
        var foto = File("")
        val entrada = File("src/main/kotlin/com/example/JSon/fotos.json").readLines()
        for(i in entrada){
            val iToObject = Json.decodeFromString<Foto>(i)
            if(iToObject.id == id){
                foto = File(iToObject.ruta)
                break
            }
        }
        return foto
    }
    //LISTA DE JUEGOS PARA JUGAR
    fun getAllGames(usuario: String): List<Game>{
        val listaReturn = mutableListOf<Game>()
        val entrada = File("src/main/kotlin/com/example/JSon/games.json").readLines()
        for(i in entrada){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.nameUsuario == usuario && !iToObject.jugado){
                listaReturn.add(iToObject)
            }
        }
        return listaReturn
    }
    //LISTA DE JUEGOS JUGADOS
    fun getAllPlayedGames(userName: String): List<Game>{
        val listaReturn = mutableListOf<Game>()
        val entrada = File("src/main/kotlin/com/example/JSon/games.json").readLines()
        for(i in entrada){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.jugado && iToObject.nameUsuario == userName) listaReturn.add(iToObject)
        }
        return listaReturn
    }
    //LISTA DE JUEGOS FAVORITOS
    fun getAllFavourites(userName: String): List<Game>{
        val listaReturn = mutableListOf<Game>()
        val entrada = File("src/main/kotlin/com/example/JSon/games.json").readLines()
        for(i in entrada){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.favorito && iToObject.nameUsuario == userName) listaReturn.add(iToObject)
        }
        return listaReturn
    }
    //DETALLE JUEGO
    fun getOneGame(id: String): Game{
        val entrada = File("src/main/kotlin/com/example/JSon/games.json").readLines()
        var detalles: Game = Game("", "", "", "" , false, "", 0, false, false,"")
        for(i in entrada){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id == id){
                detalles = iToObject
                break
            }
        }
        return detalles
    }
    //LISTA DE PAGINAS DE JUEGOS
    fun getPaginasDeJuegos(): List<Game>{
        val listaReturn = mutableListOf<Game>()
        val entrada = File("src/main/kotlin/com/example/JSon/paginaDeJuegos.json").readLines()
        for(i in entrada){
            val iToObject = Json.decodeFromString<Game>(i)
            listaReturn.add(iToObject)
        }
        return listaReturn
    }
    //LISTA DE JUEGOS FILTRADA POR NOMBRE DE LA PAGINA DE DONDE VIENE
    fun getFilteredGames(nombrePagina: String, userName: String){
        //CUANDO SEPA EL NOMBRE DE LAS PAGINAS HAGO ESTE
    }

    //OBTENER USUARIO POR USERNAME
    fun getUserByUserName(userName: String): Usuarios{
        var usuario: Usuarios = Usuarios("", "")
        val entrada = File("src/main/kotlin/com/example/JSon/usuarios.json").readLines()
        for(i in entrada){
            val iToObject = Json.decodeFromString<Usuarios>(i)
            if(iToObject.userName == userName){
                usuario = iToObject
                break
            }
        }
        return usuario
    }
    //OBTENER USUARIO REGISTRADO O NO
    fun logInUser(userName: String, contrasenya: String): Boolean{
        val entrada = File("src/main/kotlin/com/example/JSon/usuarios.json").readLines()
        for(i in entrada){
            val iToObject = Json.decodeFromString<Usuarios>(i)
            if(iToObject.userName == userName && iToObject.password == contrasenya){
                return true
            }
        }
        return false
    }

    //OBTENER SIGUIENTE ID
    fun getNextId(): String{
        var infoReturn = ""
        val listaDeNums = mutableListOf<Int>()
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json").readLines()
        val num = archivoJson.lastIndex+1
        for(i in 1..num){
            listaDeNums.add((i))
        }
        for(i in archivoJson){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id.toInt() in listaDeNums){
                listaDeNums.remove(iToObject.id.toInt())
            }
        }
        if(listaDeNums.isNotEmpty()){
            infoReturn = listaDeNums[0].toString()
            return infoReturn
        }
        infoReturn = (num+1).toString()
        return infoReturn
    }
    //SABER SI EL USUARIO YA EXISTE
    fun userAlredyExists(userName: String): Boolean{
        var infoReturn = false
        val archivoJson = File("src/main/kotlin/com/example/JSon/usuarios.json").readLines()
        for(i in archivoJson){
            val iToObject = Json.decodeFromString<Usuarios>(i)
            if(iToObject.userName == userName){
                infoReturn = true
                return infoReturn
            }
        }
        return infoReturn
    }

    //SABER SI EXISTE UN JUEGO POR ID
    fun gameExists(id: String): Boolean{
        val archivoJson = File("src/main/kotlin/com/example/JSon/games.json").readLines()
        for(i in archivoJson){
            val iToObject = Json.decodeFromString<Game>(i)
            if(iToObject.id == id){
                return true
            }
        }
        return false
    }
}