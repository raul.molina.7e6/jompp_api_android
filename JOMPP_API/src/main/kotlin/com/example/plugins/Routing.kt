package com.example.plugins

import API.JOMPP_API.src.main.kotlin.com.example.Routes.pagesRouting
import com.example.Routes.gamesRouting
import com.example.Routes.usersRoutes
import com.example.model.CustomPrincipal
import com.example.model.jsonToMap
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        gamesRouting()
        pagesRouting()
        usersRoutes()
    }
}
