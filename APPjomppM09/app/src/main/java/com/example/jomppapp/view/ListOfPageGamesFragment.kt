package com.example.jomppapp.view

import android.os.Bundle
import android.text.Layout.Directions
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jomppapp.Adapter.GameAdapter
import com.example.jomppapp.Adapter.MyOnClickListener
import com.example.jomppapp.Adapter.PageAdapter
import com.example.jomppapp.Adapter.PageOnClickListener
import com.example.jomppapp.R
import com.example.jomppapp.databinding.FragmentListOfPageGamesBinding
import com.example.jomppapp.databinding.FragmentListOfUserGamesBinding
import com.example.jomppapp.model.Game
import com.example.jomppapp.model.PaginaDeJuegos
import com.example.jomppapp.viewModel.GameViewModel


class ListOfPageGamesFragment : Fragment(), PageOnClickListener {

    private lateinit var pageAdapter: PageAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentListOfPageGamesBinding
    lateinit var gameViewModel: GameViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        gameViewModel = ViewModelProvider(requireActivity())[GameViewModel::class.java]
        binding = FragmentListOfPageGamesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gameViewModel.getJuegosNoJugados()


        val listaDePaginas = mutableListOf<PaginaDeJuegos>(
            PaginaDeJuegos("1001 Juegos", R.drawable.juegosmiluno),
            PaginaDeJuegos("AGame", R.drawable.agame),
            PaginaDeJuegos("Friv", R.drawable.friv),
            PaginaDeJuegos("Funny Games", R.drawable.funnygames),
            PaginaDeJuegos("Kizi", R.drawable.kizi),
            PaginaDeJuegos("MiniClip", R.drawable.miniclip),
            PaginaDeJuegos("Minijuegos", R.drawable.minijuegos),
            PaginaDeJuegos("Poki", R.drawable.pioki),
            PaginaDeJuegos("Y8", R.drawable.yocho),
            PaginaDeJuegos("Otra...", R.drawable.gamelogo)
        )
        setUpRecyclerView(listaDePaginas)

    }

    fun setUpRecyclerView(lista: MutableList<PaginaDeJuegos>){
        pageAdapter = PageAdapter(lista, this)

        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = pageAdapter
        }

    }

    override fun onClick(page: PaginaDeJuegos) {
        val action = ListOfPageGamesFragmentDirections.actionFragment4ToListOfFilteredGamesByPageFragment(page.name)
        findNavController().navigate(action)
    }


}