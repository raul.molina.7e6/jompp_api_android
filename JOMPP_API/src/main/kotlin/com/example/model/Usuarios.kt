package com.example.model

import kotlinx.serialization.Serializable
import java.nio.charset.StandardCharsets.UTF_8
import java.security.MessageDigest

@Serializable
data class Usuarios(
    val userName: String,
    val password: String
)
