package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Foto(
    var id: String,
    var ruta: String
)
