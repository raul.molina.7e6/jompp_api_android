package com.example.model

import io.ktor.server.auth.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8
import java.security.MessageDigest

data class CustomPrincipal(val userName: String, val realm: String) : Principal

fun getMd5Digest(str: String): ByteArray = MessageDigest.getInstance("MD5").digest(str.toByteArray(UTF_8))

val myRealm = "Access to the '/' path"
val userTable: MutableMap<String, ByteArray> = mutableMapOf(
    "admin" to getMd5Digest("admin:$myRealm:password")
)

fun jsonToMap(){
    val lineasJson = File("src/main/kotlin/com/example/JSon/usuarios.json").readLines()
    for(i in lineasJson){
        val iToObject = Json.decodeFromString<Usuarios>(i)
        val username = iToObject.userName
        val password = iToObject.password
        if(!(userTable.containsKey(username))){
            userTable.put(username, getMd5Digest("$username:$myRealm:$password"))
        }
    }


}

